# ZenHR Job Api Test

[http://jobs-api-zenhr.herokuapp.com/](http://jobs-api-zenhr.herokuapp.com/api/v1/jobs)

## Postman Docs
[https://documenter.getpostman.com](https://documenter.getpostman.com/view/12902140/TVRdAXLC)
## Install

### Clone the repository

```shell
git clone git@bitbucket.org:islamodeh96/jobs-api.git
cd jobs-api
```

### Check your Ruby version

```shell
ruby -v
```

The ouput should start with something like `ruby 2.6.0`

If not, install the right ruby version using [rbenv](https://github.com/rbenv/rbenv) (it could take a while):

```shell
rbenv install 2.6
```

### Install dependencies

Using [Bundler](https://github.com/bundler/bundler) and [Yarn](https://github.com/yarnpkg/yarn):

```shell
bundle && yarn
```

### Initialize the database
Copy database.yml.example and edit the credentials

```shell
  cp config/database.yml.example config/database.yml;
```

```shell
rails db:create db:migrate; rails db:seed;
```

### Testing
## Run the tests
```shell
rspec spec/
```

## Serve at [http://localhost:3000](http://localhost:3000)

```shell
rails s
```
