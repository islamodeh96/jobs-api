Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'api/v1/users'

  namespace :api, default: { format: :json } do
    namespace :v1 do
      resources :jobs, only: [:index, :create, :update, :destroy] do
        resources :job_applications
      end

      resources :job_applications, only: [:index, :create, :update, :destroy]
    end
  end
end
