FactoryBot.define do
  factory :job_application do |_user|
    job_id { FactoryBot.create(:job).id }
    user_id { _user.id }
  end
end
