FactoryBot.define do
  factory :job do
    title { Faker::Job.title }
    description { Faker::Job.employment_type }
  end
end
