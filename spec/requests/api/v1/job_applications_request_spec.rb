require 'rails_helper'

RSpec.describe Api::V1::JobApplicationsController, type: :request do
  before(:each) do
    @user.update(admin: false)
    @sign_in_url = user_session_path
    @login_params = {
      email: @user.email,
      password: @user.password
    }

    post @sign_in_url, params: @login_params, as: :json
    expect(response).to have_http_status(:ok)
    @authenticated_headers = response.headers.with_indifferent_access.slice("access-token", "uid", "expiry", "client", "token-type")
  end

  before(:all) do
    @user = FactoryBot.create(:user)
    @job = FactoryBot.create(:job)
    10.times { FactoryBot.create(:job_application, job_id: @job.id, user_id: FactoryBot.create(:user).id) }
  end

  describe "Inheritance" do
    it "should be inherited from Api::V1::BaseController" do
      expect(Api::V1::JobApplicationsController.ancestors.include?(Api::V1::BaseController)).to be_truthy
    end

    context "Authentication" do
      it { should use_before_action(:authenticate_user!) }
    end
  end

  describe "All job applications" do
    it "return 401 if the user is not authenticated" do
      get api_v1_job_applications_path
      expect(response).to have_http_status(:unauthorized)
    end

    it "return all the job applications created by user" do
      get api_v1_job_applications_path, headers: @authenticated_headers
      expect(response).to have_http_status(:ok)
      result = JSON.parse(response.body).with_indifferent_access
      expect(result[:success]).to be_truthy
      expect(result[:errors]).to be_empty
      expect(result[:job_applications]).to eq @user.job_applications.all.map(&:attributes).as_json
    end
  end

  describe "Create a job application" do
    before(:each) do
      @new_job_application = FactoryBot.build(:job_application, user_id: @user.id, job_id: @job.id)
    end

    it "return 401 if the user is not authenticated" do
      post api_v1_job_applications_path
      expect(response).to have_http_status(:unauthorized)
    end

    it "create the job application" do
      post api_v1_job_applications_path, headers: @authenticated_headers, params: { job_application: @new_job_application.attributes }
      expect(response).to have_http_status(:created)
      result = JSON.parse(response.body).with_indifferent_access
      expect(result[:success]).to be_truthy
      expect(result[:errors]).to be_empty
      sent_job_attributes = @new_job_application.attributes.compact
      expect(result[:job_application].slice(*sent_job_attributes.keys)).to eq sent_job_attributes.as_json
    end
  end

  describe "Update a job application" do
    before(:each) do
      @job_application = FactoryBot.create(:job_application, user_id: @user.id, job_id: @job.id)
    end

    it "return 401 if the user is not authenticated" do
      patch api_v1_job_application_path(@job_application.id)
      expect(response).to have_http_status(:unauthorized)
    end

    it "return unauthorized if the user doesn't own the job application" do
      @job_application = JobApplication.where.not(user_id: @user.id).first
      patch api_v1_job_application_path(@job_application.id), headers: @authenticated_headers
      expect(response).to have_http_status(:unauthorized)
    end

    it "update job application if the user owns it" do
      patch api_v1_job_application_path(@job_application.id), headers: @authenticated_headers, params: { job_application: @job_application.attributes }
      expect(response).to have_http_status(:no_content)
    end
  end

  describe "Destroy a job application" do
    before(:each) do
      @job_application = FactoryBot.create(:job_application, user_id: @user.id, job_id: @job.id)
    end

    it "return 401 if the user is not authenticated" do
      delete api_v1_job_application_path(@job_application.id)
      expect(response).to have_http_status(:unauthorized)
    end

    it "return unauthorized if the user doesn't own the job application" do
      @job_application = JobApplication.where.not(user_id: @user.id).first
      delete api_v1_job_application_path(@job_application.id), headers: @authenticated_headers
      expect(response).to have_http_status(:unauthorized)
    end

    it "delete the job application if the user owns it" do
      delete api_v1_job_application_path(@job_application.id), headers: @authenticated_headers
      expect(response).to have_http_status(:no_content)
    end
  end
end
