require 'rails_helper'

RSpec.describe "Api::V1::Bases", type: :controller do
  describe Api::V1::BaseController do
    context "Authentication" do
      it { should use_before_action(:authenticate_user!) }
    end
  end
end
