require 'rails_helper'

RSpec.describe Api::V1::JobsController, type: :request do
  before(:each) do
    @user = FactoryBot.create(:user)
    @sign_in_url = user_session_path
    @login_params = {
      email: @user.email,
      password: @user.password
    }

    post @sign_in_url, params: @login_params, as: :json
    expect(response).to have_http_status(:ok)
    @authenticated_headers = response.headers.with_indifferent_access.slice("access-token", "uid", "expiry", "client", "token-type")
  end

  before(:all) do
    10.times { FactoryBot.create(:job) }
  end

  describe "Inheritance" do
    it "should be inherited from Api::V1::BaseController" do
      expect(Api::V1::JobsController.ancestors.include?(Api::V1::BaseController)).to be_truthy
    end

    context "Authentication" do
      it { should use_before_action(:authenticate_user!) }
    end
  end

  describe "All jobs" do
    it "return 401 if the user is not authenticated" do
      get api_v1_jobs_path
      expect(response).to have_http_status(:unauthorized)
    end

    it "return all the jobs to the user" do
      get api_v1_jobs_path, headers: @authenticated_headers
      expect(response).to have_http_status(:ok)
      result = JSON.parse(response.body).with_indifferent_access
      expect(result[:success]).to be_truthy
      expect(result[:errors]).to be_empty
      expect(result[:jobs]).to eq Job.all.map(&:attributes).as_json
    end
  end

  describe "Create a job" do
    before(:each) do
      @new_job = FactoryBot.build(:job)
    end

    it "return 401 if the user is not authenticated" do
      post api_v1_jobs_path
      expect(response).to have_http_status(:unauthorized)
    end

    it "return unauthorized if the user is not admin" do
      post api_v1_jobs_path, headers: @authenticated_headers, params: { job: @new_job.attributes }
      expect(response).to have_http_status(:unauthorized)
    end

    it "return the created job if the user is admin" do
      @user.update(admin: true)
      post api_v1_jobs_path, headers: @authenticated_headers, params: { job: @new_job.attributes }
      expect(response).to have_http_status(:created)
      result = JSON.parse(response.body).with_indifferent_access
      expect(result[:success]).to be_truthy
      expect(result[:errors]).to be_empty
      sent_job_attributes = @new_job.attributes.compact
      expect(result[:job].slice(*sent_job_attributes.keys)).to eq sent_job_attributes.as_json
    end
  end

  describe "Create a job" do
    before(:all) do
      @job = FactoryBot.create(:job)
    end

    it "return 401 if the user is not authenticated" do
      patch api_v1_job_path(@job.id)
      expect(response).to have_http_status(:unauthorized)
    end

    it "return unauthorized if the user is not admin" do
      patch api_v1_job_path(@job.id), headers: @authenticated_headers, params: { job: @job.attributes }
      expect(response).to have_http_status(:unauthorized)
    end

    it "update if the user is admin" do
      @user.update(admin: true)
      job = FactoryBot.create(:job)
      patch api_v1_job_path(@job.id), headers: @authenticated_headers, params: { job: @job.attributes }
      expect(response).to have_http_status(:no_content)
    end
  end

  describe "Destroy a job" do
    before(:all) do
      @job = FactoryBot.create(:job)
    end

    it "return 401 if the user is not authenticated" do
      delete api_v1_job_path(@job.id)
      expect(response).to have_http_status(:unauthorized)
    end

    it "return unauthorized if the user is not admin" do
      delete api_v1_job_path(@job.id), headers: @authenticated_headers
      expect(response).to have_http_status(:unauthorized)
    end

    it "update if the user is admin" do
      @user.update(admin: true)
      job = FactoryBot.create(:job)
      delete api_v1_job_path(@job.id), headers: @authenticated_headers
      expect(response).to have_http_status(:no_content)
    end
  end
end
