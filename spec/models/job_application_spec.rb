require 'rails_helper'

RSpec.describe JobApplication, type: :model do
  describe "Validations" do
    it { should validate_uniqueness_of(:user_id).scoped_to(:job_id) }
  end

  describe "Associations" do
    it { expect(subject).to belong_to(:user) }
    it { expect(subject).to belong_to(:job) }
  end

  describe "Flags" do
    it "should use the first bit for seen flag" do
      subject.flags = 1
      expect(subject.seen?).to eq true
      expect(subject.not_seen?).to eq false
    end
  end
end
