require 'rails_helper'

RSpec.describe User, type: :model do
  describe "Associations" do
    it { expect(subject).to have_many(:job_applications).dependent(:destroy) }
  end

  describe "Flags" do
    it "should use the first bit for admin flag" do
      subject.flags = 1
      expect(subject.admin?).to eq true
      expect(subject.not_admin?).to eq false
    end
  end

  describe "Abilities" do
    context "Admin" do
      it "Should have full access on jobs" do
        user = FactoryBot.create(:admin)
        ability = Ability.new(user)
        expect(ability).to be_able_to(:manage, Job.new)
      end

      it "Should have full access on job applications" do
        user = FactoryBot.create(:admin)
        ability = Ability.new(user)
        expect(ability).to_not be_able_to(:manage, JobApplication.new)
        expect(ability).to be_able_to(:read, JobApplication.new)
      end
    end

    context "Normal user" do
      it "Should only read jobs" do
        user = FactoryBot.create(:user)
        ability = Ability.new(user)
        expect(ability).to be_able_to(:read, Job.new)
        expect(ability).to_not be_able_to(:destroy, Job.new)
        expect(ability).to_not be_able_to(:update, Job.new)
      end

      it "Should have full access on job application the user owns" do
        user = FactoryBot.create(:user)
        ability = Ability.new(user)
        expect(ability).to be_able_to(:manage, JobApplication.new(user_id: user.id))
        expect(ability).to_not be_able_to(:manage, JobApplication.new(user_id: nil))
      end
    end
  end
end
