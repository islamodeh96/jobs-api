require 'rails_helper'

RSpec.describe Job, type: :model do
  describe "Validations" do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:description) }
  end

  describe "Associations" do
    it { expect(subject).to have_many(:job_applications).dependent(:destroy) }
  end
end
