class CreateJobApplications < ActiveRecord::Migration[5.2]
  def change
    create_table :job_applications do |t|
      t.integer :user_id
      t.integer :job_id
      t.integer :flags, null: false, default: 0
      t.timestamps
    end
  end
end
