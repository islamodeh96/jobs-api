class JobApplication < ApplicationRecord
  include FlagShihTzu
  validates :user_id, uniqueness: { scope: :job_id }

  belongs_to :job
  belongs_to :user
  
  ### DON'T CHANGE THE ORDER OF THE FLAGS ###
  has_flags 1 => :seen
end
