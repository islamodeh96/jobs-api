class User < ApplicationRecord
	include DeviseTokenAuth::Concerns::User
	include FlagShihTzu

	devise :database_authenticatable, :registerable,
					:recoverable, :rememberable, :trackable, :validatable

	has_many :job_applications, dependent: :destroy

	### DON'T CHANGE THE ORDER OF THE FLAGS ###
  has_flags 1 => :admin
end
