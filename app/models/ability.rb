# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    if user.admin?
      can :manage, Job
      can :read, JobApplication
    else
      can :read, Job
      can :manage, JobApplication, user_id: user.id
    end
  end
end
