module Api
  module V1
    module ResponseConcern
      extend ActiveSupport::Concern

      included do
        rescue_from CanCan::AccessDenied, with: -> (error) { exception_handler(error, :unauthorized) }
        rescue_from ActiveRecord::RecordNotFound, with: -> (error) { exception_handler(error, :not_found) }
      end

      def exception_handler(e, status=:internal_server_error)
        render_errors([e.message], status)
      end

      def render_resource(resource, status=:no_content)
        if resource.errors.present?
          render_errors(resource.errors.full_messages, :unprocessable_entity)
        else
          render json: { resource.class.name.underscore => resource.attributes, errors: [], success: true }, status: status
        end
      end

      def render_resources(resources)
        render json: { resources.klass.to_s.underscore.pluralize => resources.map(&:attributes), errors: [], success: true }
      end

      def render_errors(errors, status)
        render json: { errors: errors, success: errors.blank? }, status: status
      end
    end
  end
end