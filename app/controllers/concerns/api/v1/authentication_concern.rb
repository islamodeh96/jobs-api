module Api
  module V1
    module AuthenticationConcern
      extend ActiveSupport::Concern

      included do
        include DeviseTokenAuth::Concerns::SetUserByToken
        before_action :authenticate_user!
        load_and_authorize_resource
      end
    end
  end
end