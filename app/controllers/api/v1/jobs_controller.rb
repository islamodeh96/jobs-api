class Api::V1::JobsController < Api::V1::BaseController
  def index
    render_resources(Job.all)
  end

  def create
    job = Job.new(job_params)
    job.save
    render_resource(job, :created)
  end

  def update
    job = Job.find_by!(id: params[:id])
    job.update(job_params)
    render_resource(job)
  end

  def destroy
    job = Job.find_by!(id: params[:id])
    job.destroy
    render_resource(job)
  end

  def job_params
    params.require(:job).permit(:title, :description)
  end
end
