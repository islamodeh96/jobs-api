module Api
  module V1
    class BaseController < ActionController::API
      include Api::V1::ResponseConcern
      include Api::V1::AuthenticationConcern
    end
  end
end
