class Api::V1::JobApplicationsController < Api::V1::BaseController
  def index
    JobApplication.update_all(JobApplication.set_flag_sql(:seen, true)) if current_user.admin?
    render_resources(job_applications)
  end

  def create
    job_applicaiton = current_user.job_applications.new(job_application_params)
    job_applicaiton.save
    render_resource(job_applicaiton, :created)
  end

  def update
    job_applicaiton = job_applications.find_by!(id: params[:id])
    job_applicaiton.update(job_application_params)
    render_resource(job_applicaiton)
  end

  def destroy
    job_applicaiton = job_applications.find_by!(id: params[:id])
    job_applicaiton.destroy
    render_resource(job_applicaiton)
  end

  private

  def job_applications
    if current_user.admin?
      JobApplication.all
    else
      current_user.job_applications
    end
  end

  def job_application_params
    params.require(:job_application).permit(:job_id)
  end
end
